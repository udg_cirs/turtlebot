# TURTLEBOT

This package provides all the basic components for running the new turtlebot with ROS.

## What's new?

### Hardware:
The new robots incorporate:

- a kobuki base
- a 360º Lidar (RPLidar)
- a RGBD stereo camera (Realsense d435i) or a webcam
- a 4DOF manipulator
- a raspberry pi 4

### Software:

The turtlebot packages have been rebuild to make it more clear and simple. 

*  **turtlebot_bringup** It has the launch and config files in order to run the necessary components:
    *  kobuki.launch: Launches the driver of the kobuki base
    *  rplidar.launch: Launches the driver of the rplidar
    *  realsense_camera.launch: Launches the driver of the realsense camera
    *  usb_camera.launch: Launches the driver of the webcam
    *  robot_description.launch: Loads the robot_description of your robot (it depends on the camera) and the robot state publisher
    *  rviz.launch: Runs rviz with a pre-defined setup
*  **turtlebot_description**: It contains the urdf models of the two versions of the robot (realsense or webcam)
*  **turtlebot_teleop**: It contains nodes to teleop the kobuki base using a keyboard or a controller

### Network

|name       | ip            |
|-----------|---------------|
|turtlebot1 | 192.168.31.31 |
|turtlebot2 | 192.168.31.32 |
|turtlebot3 | 192.168.31.33 |
|turtlebot4 | 192.168.31.34 |
|turtlebot5 | 192.168.31.35 |

These ips and names are fixed, no mather to which router you connect the turtlebot. So you can configure your /etc/hosts once for all turtlebots.

### ROS architecture

The best you can do to understand the software architecture is to run at least the robot_description.launch and kobuki.launch and do: rosrun rqt_graph rqt_graph and/or rostopic list 

All topics and nodes are encapsulated in a namespace /turtlebotX/ , where X is the id number of the turtlebot. For more information about ros namespaces, see (http://wiki.ros.org/Names).

#### Kobuki


The kobuki base receives velocity commands at /turtlebot/cmd_vel (CHECK). Since we might want to have multiple controllers, a command multiplexer is placed in between. This multiplexer Arbitrates incoming cmd_vel messages from several topics, allowing one topic at a time to command the robot, based on priorities. To learn more how the multiplexer works, see (http://wiki.ros.org/cmd_vel_mux). By default, we assume 3 inputs: Safety, teleoperation, and navigation. Generally, you can start by using the navigation topic to control the robot. Inputs and priorities can be configured in a yaml file, found in the param folder.

By default, the kobuki node computes odometry based on motor encoders. Since it has a gyroscope which is quite precise, by default gyro is used for the heading. However, this can be disabled in the kobuki_node.yaml file, if, for example, you want to fuse sensors in a more sophisitcated way (e.g., EKF). Odometry information is published to /turtlebotX/kobuki/odom and imu_data to /turtlebotX/sensors/imu_data. For more details about the rest of publishers/subscribers ,see (http://wiki.ros.org/kobuki_node).

#### RPlidar

The RPLidar publishes scan information at /turtlebotX/rplidar/scan

#### ARM (TODO)

## INSTALLATION


First, either install manually the dependencies (robot & computer): 

```bash
sudo apt install -y \
ros-noetic-joy \
ros-noetic-kobuki-* \
ros-noetic-ecl-streams \
ros-noetic-base-local-planner \
ros-noetic-sound-play \
ros-noetic-rgbd-launch \
ros-noetic-libuvc-* \
ros-noetic-realsense2-camera \
ros-noetic-realsense2-description
```

or wait after cloning all respositories and use rosdep to install dependencies (they are listed in the package.xml!)

```bash
rosdep install --from-paths src --ignore-src -r -y
```

The robot requires these packages, which should be already installed:

```bash
# Clone required packages
cd ~/catkin_ws/src
git clone https://bitbucket.org/udg_cirs/turtlebot.git # This repository!
git clone https://bitbucket.org/udg_cirs/turtlebot_description.git

#The kobuki mobile base
git clone https://bitbucket.org/udg_cirs/kobuki.git
git clone https://bitbucket.org/udg_cirs/kobuki_description.git
git clone https://bitbucket.org/udg_cirs/yujin_ocs.git

# The manipulator
git clone https://bitbucket.org/udg_cirs/swiftpro.git
git clone https://bitbucket.org/udg_cirs/swiftpro_description.git

# The Lidar
git clone https://github.com/Slamtec/rplidar_ros.git
```

In your computer, you need these packages:

```bash
# Clone required packages
cd ~/catkin_ws/src
git clone https://bitbucket.org/udg_cirs/turtlebot_desktop.git 
git clone https://bitbucket.org/udg_cirs/turtlebot_description.git 

#The kobuki mobile base
git clone https://bitbucket.org/udg_cirs/kobuki_desktop.git
git clone https://bitbucket.org/udg_cirs/kobuki_description.git
git clone https://bitbucket.org/udg_cirs/yujin_ocs.git

# The manipulator
git clone https://bitbucket.org/udg_cirs/swiftpro_desktop.git
git clone https://bitbucket.org/udg_cirs/swiftpro_description.git

# The simulation world
git clone https://bitbucket.org/udg_cirs/small_house_world.git 
```


## How to run

In the robot (through ssh) execute in multiple terminals (use byobu):

```bash
# Run roscore
roscore
```
```bash
# load robot description
roslaunch turtlebot_bringup robot_description.launch
```
```bash
# launch kobuki driver
roslaunch turtlebot_bringup kobuki.launch
```
```bash
# launch rplidar driver
roslaunch turtlebot_bringup rplidar.launch
```
```bash
# launch camera
roslaunch turtlebot_bringup realsense_camera.launch
```
```bash
# launch manipulator driver
roslaunch turtlebot_bringup swiftpro_uarm.launch
```


In your computer, run (Remember to set the ROS_MASTER_URI!):

```bash
# launch rviz
roslaunch turtlebot_desktop rviz.launch
```

And some teleoperation for the base, for example:
```bash
# launch kobuki driver
roslaunch turtlebot_destktop keyboard_teleop.launch
```


## Simulation

refer to the README.md file in turtlebot_destktop for simulation instructions